# wredirect

## Description

wredirect is a [Weather Underground](https://www.wunderground.com/)
(wunderground) redirection service. Given a zip code, wredirect redirects to the
10-day forecast for that zip code.

Previously, Weather Underground would do this automatically. Unfortunately, a
site update broke this functionality a while back.

## Maintainer

[Jacob Henner](https://jacobhenner.com)
