FROM python:3.8

COPY pyproject.toml poetry.lock ./
COPY wredirect wredirect/

RUN pip install poetry
RUN poetry install

CMD poetry run wredirect