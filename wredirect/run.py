#!/usr/bin/env python3

import os
import responder
import requests

api = responder.API()

WEATHER_API_URL = "https://api.weather.com/v3/location/search"
DEFAULT_API_PARAMS = {
    "apiKey": os.environ["WEATHER_API_KEY"],
    "language": "en-US",
    "locationType": "postCode",
    "format": "json",
}


@api.route("/zip/{zipcode}")
async def weather_redirect(req, resp, *, zipcode):
    # TODO: Consider adding a cache
    weather_api_response = requests.get(
        WEATHER_API_URL, params={**DEFAULT_API_PARAMS, **{"query": zipcode}}
    ).json()

    weather_api_data = weather_api_response["location"]
    state = await format(weather_api_data["adminDistrictCode"][0])
    country = await format(weather_api_data["countryCode"][0])
    municipality = await format(weather_api_data["city"][0])

    resp.status_code = api.status_codes.HTTP_302
    resp.headers[
        "Location"
    ] = f"https://www.wunderground.com/forecast/{country}/{state}/{municipality}/{zipcode}"


async def format(label: str) -> str:
    return label.lower().replace(" ", "-")


def main():
    api.run()


if __name__ == "__main__":
    main()
